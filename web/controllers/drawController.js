
var lan;
var app = angular.module('myApp211', []);

var t = 0;
app.controller('draw', function ($scope, $http)


{
    
        $scope.drawPath = function () {
    
     
 
         
          $http.get("webresources/map.rastidesc?id=" + $scope.selected2)
            .then(function (response1) {
                var lat2 = parseFloat(response1.data[0].lat);
                var lot = parseFloat(response1.data[0].lon);
                //alustetaan kartta
                var mapProp = {
                    center: new google.maps.LatLng(parseFloat(response1.data[0].lat).toFixed(5), parseFloat(response1.data[0].lon).toFixed(5)),
                    zoom: 15
                };



                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

                for (var i1 = 0; i1 < response1.data.length; i1++) {
                    try {
                        var lat0 = 0.0 + parseFloat(response1.data[i1].lat);
                        var lon0 = 0.0 + parseFloat(response1.data[i1].lon);
                        var lat1 = 0.0 + parseFloat(response1.data[i1 + 1].lat);
                        var lon1 = 0.0 + parseFloat(response1.data[i1 + 1].lon);


                        var flightPlanCoordinates = [
                            {lat: lat0, lng: lon0},
                            {lat: lat1, lng: lon1}



                        ];


                        var flightPath = new google.maps.Polyline({
                            path: flightPlanCoordinates,
                            geodesic: false,
                            strokeColor: '#FF0000',
                            strokeOpacity: 1.0,
                            strokeWeight: 5
                        });


                        flightPath.setMap(map);
                    } catch (e) {

                    }
                }



                for (var i = 0; i < response1.data.length; i++) {
                    lan = parseFloat(response1.data[0].lat).toFixed(5);
                    var cityCircle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                        center: new google.maps.LatLng(parseFloat(response1.data[i].lat).toFixed(5), parseFloat(response1.data[i].lon).toFixed(5)),
                        radius: 50
                    });

                    var markerIcon = {
                        url: 'http://image.flaticon.com/icons/svg/252/252025.svg',
                        scaledSize: new google.maps.Size(80, 80),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(32, 65),
                        labelOrigin: new google.maps.Point(40, 33)
                    };



                    var markerLabel = response1.data[i].name;
                    var marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(parseFloat(response1.data[i].lat).toFixed(5), parseFloat(response1.data[i].lon).toFixed(5)),
                        label: {
                            text: markerLabel,
                            color: "#FFFF44",
                            fontSize: "16px",
                            fontWeight: "bold"
                        }
                    });






                }


            });

        };
    
     $http.get("webresources/map.routes")
                .then(function (response1) {
           
                    $scope.routeData = response1.data;
            
              
                });
                
         
                

   





//Aseta rajat


});
