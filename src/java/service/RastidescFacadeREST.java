/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import map.Rastidesc;

/**
 *
 * @author e1197
 */
@javax.ejb.Stateless
@Path("map.rastidesc")
public class RastidescFacadeREST extends AbstractFacade<Rastidesc> {

    @PersistenceContext(unitName = "WebApplication13PU")
    private EntityManager em = Persistence.createEntityManagerFactory("WebApplication13PU").createEntityManager();

    public RastidescFacadeREST() {
        super(Rastidesc.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public Response create(Rastidesc entity) {
        super.create(entity);

        em.merge(entity);
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();

        super.create(entity);
        return Response.ok("OK", MediaType.TEXT_PLAIN).build();
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Rastidesc entity) {
        super.edit(entity);
    }

    @DELETE
       @Path("{id}")
    public Response remove(@PathParam("id") Integer id) {

        try {
            em.getTransaction().begin();
            Rastidesc item=em.find(Rastidesc.class, id);
            em.remove(item);
          em.getTransaction().commit();
            

            return Response.ok("OK id=" , MediaType.TEXT_PLAIN).build();

        } catch (Exception e) {
             return Response.ok("error id not found" , MediaType.TEXT_PLAIN).build();
        }
    }

    @GET

    @Override
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public List<Rastidesc> findAll(@QueryParam("id") Integer id) {

        try {
            int id1 = id;

            return em.createNativeQuery(" SELECT * FROM Rastidesc r where Routeid=" + id1, Rastidesc.class).getResultList();
        } catch (Exception e) {
            return super.findAll();
        }
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Rastidesc> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        List<Rastidesc> list = super.findAll();

        double lat1 = 180;

        double lon1 = 180;
        for (Rastidesc item : list) {

            double item_Lat = Double.parseDouble(item.getLat());
            if (item_Lat < lat1) {
                lat1 = item_Lat;

            }

        }

        return String.valueOf(lat1);
    }

       @GET
    @Path("getDimensionsMax")
    @Produces(MediaType.TEXT_PLAIN)
    public String getDimensionsMax() {

        List<Rastidesc> list =  em.createNativeQuery(" SELECT * FROM Rastidesc r where Routeid=" + 1, Rastidesc.class).getResultList();

        double lat1 = -180;

        double lon1 = -180;
        for (Rastidesc item : list) {

            try {
                float item_Lat = (float) Float.parseFloat(item.getLat());

                if (item_Lat > lat1) {
                    lat1 = item_Lat;

                }

            } catch (Exception e) {

            }

            try {
                float item_lot = (float) Float.parseFloat(item.getLon());

                if (item_lot > lon1) {
                    lon1 = item_lot;

                }

            } catch (Exception e) {

            }
        }

        return String.valueOf(lat1  +","+ lon1);
    }
    
    @GET
    @Path("getDimensionsMin")
    @Produces(MediaType.TEXT_PLAIN)
    public String getDimensionsMin() {

        List<Rastidesc> list =    em.createNativeQuery(" SELECT * FROM Rastidesc r where Routeid=" + 1, Rastidesc.class).getResultList();;

        double lat1 = 180;

        double lon1 = 180;
        for (Rastidesc item : list) {

            try {
                float item_Lat = (float) Float.parseFloat(item.getLat());

                if (item_Lat < lat1) {
                    lat1 = item_Lat;

                }

            } catch (Exception e) {

            }

            try {
                float item_lot = (float) Float.parseFloat(item.getLon());

                if (item_lot < lon1) {
                    lon1 = item_lot;

                }

            } catch (Exception e) {

            }
        }

        return String.valueOf( lat1  +","+ lon1);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
