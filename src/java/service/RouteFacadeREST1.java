/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import map.Rastidesc;
import map.Routename2;

/**
 *
 * @author e1197
 */
@javax.ejb.Stateless
@Path("map.routes")
public class RouteFacadeREST1 extends AbstractFacade<Routename2> {

    @PersistenceContext(unitName = "WebApplication13PU")
    private EntityManager em = Persistence.createEntityManagerFactory("WebApplication13PU").createEntityManager();

    public RouteFacadeREST1() {
        super(Routename2.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public Response create(Routename2 entity) {
        super.create(entity);

        em.merge(entity);
        em.getTransaction().begin();
        em.persist(entity);

        em.getTransaction().commit();

        super.create(entity);
        Response response = Response.ok("OK", MediaType.TEXT_PLAIN).build();
        response.getHeaders().add("Access-Control-Allow-Origin", "*");

        return response;
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Routename2 entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public Response remove(@PathParam("id") Integer id) {

        try {
            
            em.getTransaction().begin();
            Routename2 entity = getEntityManager().getReference(Routename2.class, id);
            
         
            em.remove(entity);
            em.getTransaction().commit();

            return Response.ok("OK" , MediaType.TEXT_PLAIN).build();

        } catch (Exception e) {
            return Response.ok("error", MediaType.TEXT_PLAIN).build();
        }
    }

    @GET

    @Override
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})

    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public List<Routename2> findAll(@QueryParam("id") Integer id) {

        try {
            int id1 = id;

            return em.createNativeQuery(" SELECT * FROM Routename2 r where Routeid=" + id1, Routename2.class).getResultList();
        } catch (Exception e) {
            return super.findAll();
        }
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Routename2> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
