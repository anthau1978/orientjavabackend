/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author e1197
 */
@Entity
@Table(name = "Routename")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Routename2.findAll", query = "SELECT r FROM Routename2 r"),
    @NamedQuery(name = "Routename2.findById", query = "SELECT r FROM Routename2 r WHERE r.id = :id"),
    @NamedQuery(name = "Routename2.findByLength", query = "SELECT r FROM Routename2 r WHERE r.length = :length")})
public class Routename2 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Lob
    @Size(max = 65535)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "length")
    private Float length;

 

    public Routename2() {
    }



  public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

 



    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Routename2)) {
            return false;
        }
        Routename2 other = (Routename2) object;
 
        return true;
    }


    
}
